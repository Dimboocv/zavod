app.controller('ReportCtrl', function($scope, $routeSegment) {

    $scope.pageClass = 'reportpage';
    $scope.fests = [
      { 
        fid: 1,
        title: 'zvd1',
        judges: [
          { 
            jid: 1,
            title: 'title1 judge1 video1',
            info: 'info info info info info info info info',
            link: 'link1',
            url_img: 'url1'
          },
          { 
            jid: 2,
            title: 'title2 judge2 video2',
            info: 'info info info info info info info info',
            link: 'link2',
            url_img: 'url2'
          }
        ],
        photos: [
          { 
            pid: 1,
            title: 'zvd1 img',
            big: 'bigurl',
            small: 'smallurl'
          },
          { 
            pid: 2,
            title: 'zvd2 img',
            big: 'bigurl',
            small: 'smallurl'
          }
        ],
        nominations: [
          {
            nid: 1,
            title: 'nom1',
            battles: [
              { 
                bid: 1,
                dancer1: 'nickname1',
                dancer2: 'nickname2',
                link: 'videolink',
                imgpreview: 'imgurl',
                plan: 'final',
                classplan: 'final'
              },
              { 
                bid: 1,
                dancer1: 'nickname1',
                dancer2: 'nickname2',
                link: 'videolink',
                imgpreview: 'imgurl',
                plan: '1/2',
                classplan: 'semifinal'
              },
              { 
                bid: 2,
                dancer1: 'nickname3',
                dancer2: 'nickname4',
                link: 'videolink',
                imgpreview: 'imgurl',
                plan: '1/4 ',
                classplan: 'other'
              },
              { 
                bid: 2,
                dancer1: 'nickname3',
                dancer2: 'nickname4',
                link: 'videolink',
                imgpreview: 'imgurl',
                plan: '1/8 ',
                classplan: 'other'
              }
            ]
          },
          {
            nid: 2,
            title: 'nom2'
          }
        ]
      },
      { 
        fid: 2,
        title: 'zvd2',
        judges: [
          { 
            jid: 1,
            title: '123123 title1 judge1 video1',
            info: '123123 info info info info info info info info',
            link: 'link1',
            url_img: '13123 url1'
          },
          { 
            jid: 2,
            title: '123123 title2 judge2 video2',
            info: '123123 info info info info info info info info',
            link: '1323link2',
            url_img: '123123 url2'
          }
        ],
        nominations: [
          {
            nid: 1,
            title: 'nom1'
          },
          {
            nid: 2,
            title: 'nom2'
          }
        ]
      },
      { 
        fid: 3,
        title: 'zvd3',
        nominations: [
          {
            nid: 1,
            title: 'nom1'
          },
          {
            nid: 2,
            title: 'nom2'
          }
        ]
      },
      {
        fid: 4,
        title: 'zvd4',
        nominations: [
          {
            nid: 1,
            title: 'nom1'
          },
          {
            nid: 2,
            title: 'nom2'
          }
        ]
      }
    ];
    $scope.$routeSegment = $routeSegment;
    
});