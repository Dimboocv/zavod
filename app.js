var app = angular.module('app', ['ngRoute', 'ngAnimate', 'route-segment', 'view-segment']);

app.config(function($routeSegmentProvider, $routeProvider) {
    
    // Configuring provider options
    
    $routeSegmentProvider.options.autoLoadTemplates = true;
    
    // Setting routes. This consists of two parts:
    // 1. `when` is similar to vanilla $route `when` but takes segment name instead of params hash
    // 2. traversing through segment tree to set it up
  
    $routeSegmentProvider
    
        .when('/home',              'zvd1')
        
        .when('/trailer',           'zvd2')
        
        .when('/info',              'zvd3')
        .when('/info/nomination',   'zvd3.nomination')
        .when('/info/registration', 'zvd3.registration')
        .when('/info/map',          'zvd3.map')
        
        .when('/report',                                'zvd4')
        .when('/report/vol.:fid',                       'zvd4.fest')
        .when('/report/vol.:fid/photo',                 'zvd4.fest.photo')
        .when('/report/vol.:fid/photo/:pid',            'zvd4.fest.photo.onephoto')
        .when('/report/vol.:fid/judge',                 'zvd4.fest.judge')
        .when('/report/vol.:fid/judge/video:jid',       'zvd4.fest.judge.showcase')
        .when('/report/vol.:fid/battle:nid',            'zvd4.fest.battle')
        .when('/report/vol.:fid/battle:nid/video:bid',  'zvd4.fest.battle.video')
            
        .segment('zvd1', {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'})
            
        .segment('zvd2', {
            templateUrl: 'templates/trailer.html',
            controller: 'TrailerCtrl'})
        
        .segment('zvd3', {
            templateUrl: 'templates/info.html',
            controller: ''})
            
        .within()
            
            .segment('timing', {
                'default': true,
                templateUrl: 'templates/info/timing.html'})
                
            .segment('nomination', {
                templateUrl: 'templates/info/nomination.html'})
            
            .segment('registration', {
                templateUrl: 'templates/info/registration.html'})
                
            .segment('map', {
                templateUrl: 'templates/info/map.html'})
                
        .up()
        
        .segment('zvd4', {
            templateUrl: 'templates/report.html',
            controller: 'MainCtrl'})
            
        .within()
            
            .segment('cap', {
                'default': true,
                templateUrl: 'templates/report/cap.html'})
            
            .segment('fest', {
                templateUrl: 'templates/report/fest.html',
                dependencies: ['fid']})
            
            .within() 
                
                .segment('judge', {
                    'default': true,
                    templateUrl: 'templates/report/fest/judge.html'})
                    
                    .within() 
                    
                    .segment('showcase', {
                        templateUrl: 'templates/report/fest/judge/showcase.html',
                        dependencies: ['jid']})
                    
                    .up()
                    
                .segment('photo', {
                    templateUrl: 'templates/report/fest/photo.html'})
                    
                    .within() 
                    
                    .segment('onephoto', {
                        templateUrl: 'templates/report/fest/photo/onephoto.html',
                        dependencies: ['pid']})
                    
                    .up()
                    
                
                .segment('battle', {
                    templateUrl: 'templates/report/fest/battles.html',
                    dependencies: ['nid']})
                    
                    .within() 
                    
                    .segment('video', {
                        templateUrl: 'templates/report/fest/battle/battle.html',
                        dependencies: ['bid']})
                    
                    .up()
                
            .up()
                
        .up()

                    
                    

    $routeProvider.otherwise({redirectTo: '/home'}); 
}) ;

app.value('loader', {show: false});

app.controller('MainCtrl', function($scope, $routeSegment, loader) {

    $scope.$routeSegment = $routeSegment;
    $scope.loader = loader;

    $scope.$on('routeSegmentChange', function() {
        loader.show = false;
    })
});

app.controller('Section1Ctrl', function($scope, $routeSegment) {
    
    $scope.$routeSegment = $routeSegment;
    $scope.test = { btnClicked: false };
    $scope.items = [ 1,2,3,4,5 ];
});

app.controller('Section1ItemCtrl', function($scope, $routeSegment) {

    $scope.$routeSegment = $routeSegment;
    $scope.item = { id: $routeSegment.$routeParams.id };
    $scope.test = { textValue: '' };
});

app.controller('Section2Ctrl', function($scope, $routeSegment) {

    $scope.$routeSegment = $routeSegment;
    $scope.test = { textValue: '' };
    $scope.items = [ 1,2,3,4,5,6,7,8,9 ];
});

app.controller('ErrorCtrl', function($scope, error) {
    $scope.error = error;
});

app.controller('SlowDataCtrl', function($scope, data, loader) {
    loader.show = false;
    $scope.data = data;
});

